package io.gitlab.liferooter.enderhole.utils

import io.gitlab.liferooter.enderhole.EnderHole
import net.minecraft.registry.Registry
import net.minecraft.util.Identifier

class RegistryWrapper<V>(private val registry: Registry<V>) {
    fun <T : V> register(id: String, entry: T): T {
        return Registry.register(registry, Identifier(EnderHole.MOD_ID, id), entry)
    }
}