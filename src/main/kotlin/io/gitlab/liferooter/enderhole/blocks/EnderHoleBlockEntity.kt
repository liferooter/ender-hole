package io.gitlab.liferooter.enderhole.blocks

import net.minecraft.block.BlockState
import net.minecraft.block.entity.BlockEntity
import net.minecraft.nbt.NbtCompound
import net.minecraft.registry.RegistryKey
import net.minecraft.registry.RegistryKeys
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.GlobalPos
import net.minecraft.world.World
import kotlin.jvm.optionals.getOrNull

class EnderHoleBlockEntity(pos: BlockPos, state: BlockState) :
    BlockEntity(ModBlocks.ENDER_HOLE_BLOCK_ENTITY, pos, state) {
    var targetPosition: GlobalPos? = null

    override fun writeNbt(nbt: NbtCompound) {
        targetPosition?.let {
            nbt.putString("dimension", it.dimension.value.toString())
            nbt.putInt("target_x", it.pos.x)
            nbt.putInt("target_y", it.pos.y)
            nbt.putInt("target_z", it.pos.z)
        }

        super.writeNbt(nbt)
    }

    override fun readNbt(nbt: NbtCompound) {
        if (nbt.contains("dimension") && nbt.contains("target_x") && nbt.contains("target_y") && nbt.contains("target_z")) {
            val dimension = RegistryKey.of(RegistryKeys.WORLD, Identifier(nbt.getString("dimension")))
            val pos = BlockPos(nbt.getInt("target_x"), nbt.getInt("target_y"), nbt.getInt("target_z"))

            targetPosition = GlobalPos.create(dimension, pos)
        }

        super.readNbt(nbt)
    }
}