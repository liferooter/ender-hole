package io.gitlab.liferooter.enderhole.blocks

import io.gitlab.liferooter.enderhole.utils.RegistryWrapper
import net.fabricmc.fabric.api.`object`.builder.v1.block.entity.FabricBlockEntityTypeBuilder
import net.minecraft.registry.Registries

object ModBlocks {
    private val BLOCKS = RegistryWrapper(Registries.BLOCK)
    private val BLOCK_ENTITIES = RegistryWrapper(Registries.BLOCK_ENTITY_TYPE)

    val ENDER_HOLE = BLOCKS.register(
        EnderHoleBlock.BLOCK_ID, EnderHoleBlock()
    )
    val ENDER_HOLE_BLOCK_ENTITY = BLOCK_ENTITIES.register(
        EnderHoleBlock.BLOCK_ID, FabricBlockEntityTypeBuilder.create(::EnderHoleBlockEntity, ENDER_HOLE).build()
    )

    fun init() {}
}