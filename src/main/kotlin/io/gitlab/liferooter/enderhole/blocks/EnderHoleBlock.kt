package io.gitlab.liferooter.enderhole.blocks

import io.gitlab.liferooter.enderhole.EnderHole
import net.fabricmc.fabric.api.`object`.builder.v1.block.FabricBlockSettings
import net.minecraft.block.Block
import net.minecraft.block.BlockEntityProvider
import net.minecraft.block.BlockState
import net.minecraft.block.Blocks
import net.minecraft.block.entity.BlockEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.CompassItem
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.network.packet.s2c.play.PositionFlag
import net.minecraft.sound.BlockSoundGroup
import net.minecraft.sound.SoundCategory
import net.minecraft.sound.SoundEvents
import net.minecraft.state.StateManager
import net.minecraft.state.property.EnumProperty
import net.minecraft.text.Text
import net.minecraft.util.ActionResult
import net.minecraft.util.Formatting
import net.minecraft.util.Hand
import net.minecraft.util.StringIdentifiable
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.GlobalPos
import net.minecraft.util.math.Vec3i
import net.minecraft.world.World
import net.minecraft.world.dimension.DimensionTypes
import net.minecraft.world.event.GameEvent
import kotlin.jvm.optionals.getOrNull

class EnderHoleBlock : Block(SETTINGS), BlockEntityProvider {
    companion object {
        const val BLOCK_ID = "ender_hole"

        val STATE = EnumProperty.of("hole_state", HoleState::class.java)!!
        val SETTINGS = FabricBlockSettings.create().requiresTool()
            .strength(Blocks.ANCIENT_DEBRIS.hardness, Blocks.ANCIENT_DEBRIS.blastResistance)
            .sounds(BlockSoundGroup.AMETHYST_BLOCK).luminance {
                when (it.get(STATE)!!) {
                    HoleState.UNLINKED -> 0
                    HoleState.ONE_SIDE_LINKED -> 8
                    HoleState.LINKED -> 15
                }
            }!!

        fun getEntity(world: World, pos: BlockPos): EnderHoleBlockEntity? =
            world.getBlockEntity(pos, ModBlocks.ENDER_HOLE_BLOCK_ENTITY).getOrNull()
    }

    enum class HoleState(private val stateName: String) : StringIdentifiable {
        UNLINKED("unlinked"), ONE_SIDE_LINKED("one_side_linked"), LINKED("linked");

        override fun asString(): String = stateName
    }

    private enum class CompassUseResult {
        LINKED {
            override val messageId: String = "hole_linked_message"
            override val color: Formatting = Formatting.GREEN
            override val isLinked: Boolean = true
        },

        ONE_SIDE_LINKED {
            override val messageId: String = "one_side_linked_message"
            override val color: Formatting = Formatting.WHITE
            override val isLinked: Boolean = true
        },

        OTHER_SIDE_NOT_HOLE {
            override val messageId: String = "other_side_not_hole"
        },

        ALREADY_LINKED {
            override val messageId: String = "already_linked_message"
        },

        COMPASS_NOT_LINKED {
            override val messageId: String = "compass_not_linked_message"
        },

        UNKNOWN_DIMENSION {
            override val messageId: String = "non_existing_dimension"
        },

        LODESTONE_NOT_FOUND {
            override val messageId: String = "lodestone_not_found_message"
        },

        BOTH_IN_END {
            override val messageId: String = "both_in_end_message"
        },

        NEITHER_IN_END {
            override val messageId: String = "neither_in_end_message"
        };

        open val isLinked: Boolean = false
        open val color: Formatting = Formatting.YELLOW

        abstract val messageId: String

        fun sendMessage(player: PlayerEntity) {
            player.sendMessage(
                Text.translatable("block.${EnderHole.MOD_ID}.${messageId}")
                    .styled { it.withColor(color) }
            )
        }
    }

    init {
        defaultState = defaultState.with(STATE, HoleState.UNLINKED)
    }

    override fun appendProperties(builder: StateManager.Builder<Block, BlockState>) {
        builder.add(STATE)
    }

    override fun createBlockEntity(pos: BlockPos, state: BlockState): BlockEntity {
        return EnderHoleBlockEntity(pos, state)
    }

    @Deprecated("deprecated in parent class")
    override fun onUse(
        state: BlockState, world: World, pos: BlockPos, player: PlayerEntity, hand: Hand, hit: BlockHitResult
    ): ActionResult {
        val itemStack = player.getStackInHand(hand)

        if (itemStack.isOf(Items.COMPASS)) {
            val result = onUseCompass(state, world, pos, itemStack) ?: return ActionResult.CONSUME_PARTIAL

            if (result.isLinked) world.playSound(
                player, pos, SoundEvents.ITEM_LODESTONE_COMPASS_LOCK, SoundCategory.BLOCKS, 1.0F, 1.0F
            )

            if (!world.isClient)
                result.sendMessage(player)

            return ActionResult.SUCCESS
        } else {
            return teleportPlayer(state, world, pos, player)
        }
    }

    private fun onUseCompass(
        state: BlockState, world: World, pos: BlockPos, compass: ItemStack
    ): CompassUseResult? {
        if (state.get(STATE) == HoleState.LINKED) return CompassUseResult.ALREADY_LINKED

        val nbt = compass.nbt ?: return CompassUseResult.COMPASS_NOT_LINKED
        val lodestonePos = CompassItem.createLodestonePos(nbt) ?: return CompassUseResult.COMPASS_NOT_LINKED

        val isEnd = world.dimensionKey.value == DimensionTypes.THE_END_ID
        val isLodestoneInEnd = lodestonePos.dimension.value == DimensionTypes.THE_END_ID

        if (isEnd && isLodestoneInEnd) return CompassUseResult.BOTH_IN_END
        if (!isEnd && !isLodestoneInEnd) return CompassUseResult.NEITHER_IN_END

        val server = world.server ?: return null
        val targetWorld = server.getWorld(lodestonePos.dimension) ?: return CompassUseResult.UNKNOWN_DIMENSION

        val lodestone = targetWorld.getBlockState(lodestonePos.pos)

        if (!lodestone.isOf(Blocks.LODESTONE)) return CompassUseResult.LODESTONE_NOT_FOUND

        val otherHolePos = lodestonePos.pos.add(Vec3i(0, 1, 0))
        val otherHole = getEntity(targetWorld, otherHolePos)
            ?: return CompassUseResult.OTHER_SIDE_NOT_HOLE

        val thisHole = getEntity(world, pos)!!
        thisHole.targetPosition = GlobalPos.create(lodestonePos.dimension, otherHolePos)

        if (otherHole.targetPosition == GlobalPos.create(world.registryKey, pos)) {
            val newState = state.with(STATE, HoleState.LINKED)
            world.setBlockState(pos, newState)
            world.emitGameEvent(GameEvent.BLOCK_CHANGE, pos, GameEvent.Emitter.of(newState))

            val otherHoleState = targetWorld.getBlockState(otherHolePos).with(STATE, HoleState.LINKED)
            targetWorld.setBlockState(otherHolePos, otherHoleState)
            world.emitGameEvent(GameEvent.BLOCK_CHANGE, pos, GameEvent.Emitter.of(otherHoleState))

            return CompassUseResult.LINKED
        } else {
            val newState = state.with(STATE, HoleState.ONE_SIDE_LINKED)
            world.setBlockState(pos, newState)

            return CompassUseResult.ONE_SIDE_LINKED
        }
    }

    @Deprecated("deprecated in parent class")
    override fun onStateReplaced(
        state: BlockState,
        world: World,
        pos: BlockPos,
        newState: BlockState,
        moved: Boolean
    ) {
        if (newState.isOf(ModBlocks.ENDER_HOLE) && !moved) return
        val server = world.server ?: return
        if (state.get(STATE) != HoleState.LINKED) return

        val entity = getEntity(world, pos)!!
        val target = entity.targetPosition!!
        val targetWorld = server.getWorld(target.dimension)!!
        val targetState = targetWorld.getBlockState(target.pos).with(STATE, HoleState.ONE_SIDE_LINKED)

        targetWorld.setBlockState(target.pos, targetState)
        targetWorld.emitGameEvent(GameEvent.BLOCK_CHANGE, target.pos, GameEvent.Emitter.of(targetState))
    }

    private fun teleportPlayer(state: BlockState, world: World, pos: BlockPos, player: PlayerEntity): ActionResult {
        if (state.get(STATE) != HoleState.LINKED) {
            if (world.isClient)
                player.sendMessage(Text.translatable("block.${EnderHole.MOD_ID}.cannot_teleport_not_linked")
                    .styled { it.withColor(Formatting.YELLOW) })
            return ActionResult.SUCCESS
        }

        world.playSound(player, pos, SoundEvents.BLOCK_PORTAL_TRAVEL, SoundCategory.BLOCKS, 1.0F, 1.0F)

        val server = world.server ?: return ActionResult.CONSUME_PARTIAL
        val holeEntity = getEntity(world, pos)!!
        val target = holeEntity.targetPosition!!
        val targetWorld = server.getWorld(target.dimension)!!

        player.teleport(
            targetWorld,
            target.pos.x.toDouble(),
            (target.pos.y + 1).toDouble(),
            target.pos.z.toDouble(),
            setOf(PositionFlag.X, PositionFlag.Y, PositionFlag.Z),
            player.yaw,
            player.pitch
        )

        return ActionResult.SUCCESS
    }
}