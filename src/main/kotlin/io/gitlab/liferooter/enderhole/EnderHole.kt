package io.gitlab.liferooter.enderhole

import io.gitlab.liferooter.enderhole.blocks.ModBlocks
import io.gitlab.liferooter.enderhole.items.ModItems
import net.fabricmc.api.ModInitializer
import org.slf4j.LoggerFactory

object EnderHole : ModInitializer {
	const val MOD_ID = "enderhole"
    val logger = LoggerFactory.getLogger(MOD_ID)

	override fun onInitialize() {
		ModBlocks.init()
		ModItems.init()
	}
}